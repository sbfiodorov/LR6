import java.io.Serializable;

public class Figures
{
    @Override
    public String toString()
    {
        return "Figures{}";
    }
}

class Circle extends Figures implements Shape, Serializable
{
    private final double radius;
    private Color fillColor;

    public double getRadius()
    {
        return radius;
    }

    public Color getFillColor()
    {
        return fillColor;
    }

    public Color getBorderColor()
    {
        return borderColor;
    }

    public FiguresTypes getFiguresType()
    {
        return figuresType;
    }

    private Color borderColor;
    private FiguresTypes figuresType;

    Circle(double radius, Color fillColor, Color borderColor, FiguresTypes figuresType)
    {
        this.radius = radius;
        this.fillColor = fillColor;
        this.borderColor = borderColor;
        this.figuresType = figuresType;
        if (radius <= 0)
        {
            throw new IllegalArgumentException("Parameters can't be <= 0");
        }
    }

    @Override
    public double area()
    {
        return Math.PI * Math.pow(radius, 2);
    }

    @Override
    public Color fillColor()
    {
        return fillColor;
    }

    @Override
    public Color borderColor()
    {
        return borderColor;
    }

    @Override
    public String figure()
    {
        return "Круг";
    }

    @Override
    public FiguresTypes figuresType()
    {
        return figuresType;
    }

    @Override
    public String toString()
    {
        return "Circle" +
                "{" +
                "radius=" + radius +
                ", fillColor=" + fillColor +
                ", borderColor=" + borderColor +
                '}';
    }
}

class Square extends Figures implements Shape, Serializable
{
    public double getSide()
    {
        return side;
    }

    public Color getFillColor()
    {
        return fillColor;
    }

    public Color getBorderColor()
    {
        return borderColor;
    }

    public FiguresTypes getFiguresType()
    {
        return figuresType;
    }

    private final double side;
    private Color fillColor;
    private Color borderColor;
    private FiguresTypes figuresType;

    Square(double side, Color fillColor, Color borderColor, FiguresTypes figuresType)
    {
        this.side = side;
        this.fillColor = fillColor;
        this.borderColor = borderColor;
        this.figuresType = figuresType;
        if (side <= 0)
        {
            throw new IllegalArgumentException("Parameters can't be <= 0");
        }
    }

    @Override
    public double area()
    {
        return Math.pow(side, 2);
    }

    @Override
    public Color fillColor()
    {
        return fillColor;
    }

    @Override
    public Color borderColor()
    {
        return borderColor;
    }

    @Override
    public String figure()
    {
        return "Квадрат";
    }

    @Override
    public FiguresTypes figuresType()
    {
        return figuresType;
    }

    @Override
    public String toString()
    {
        return "Square" +
                " { " +
                "side = " + side +
                ", fillColor = " + fillColor +
                ", borderColor = " + borderColor +
                " }";
    }
}

class Rectangle extends Figures implements Shape, Serializable
{
    public double getWidth()
    {
        return width;
    }

    public double getHeight()
    {
        return height;
    }

    public Color getFillColor()
    {
        return fillColor;
    }

    public Color getBorderColor()
    {
        return borderColor;
    }

    public FiguresTypes getFiguresType()
    {
        return figuresType;
    }

    private final double width;
    private final double height;
    private Color fillColor;
    private Color borderColor;
    private FiguresTypes figuresType;

    Rectangle(double width, double height, Color fillColor, Color borderColor, FiguresTypes figuresType)
    {
        this.width = width;
        this.height = height;
        this.fillColor = fillColor;
        this.borderColor = borderColor;
        this.figuresType = figuresType;
        if (width <= 0 || height <= 0)
        {
            throw new IllegalArgumentException("Parameters can't be <= 0");
        }
        if (width == height)
        {
            throw new IllegalArgumentException("Parameters can't be '==', it's a Rectangle");
        }
    }

    @Override
    public double area()
    {
        return width * height;
    }

    @Override
    public Color fillColor()
    {
        return fillColor;
    }

    @Override
    public Color borderColor()
    {
        return borderColor;
    }

    @Override
    public String figure()
    {
        return "Прямоугольник";
    }

    @Override
    public FiguresTypes figuresType()
    {
        return figuresType;
    }

    @Override
    public String toString()
    {
        return "Rectangle" +
                " { " +
                "width = " + width +
                ", height = " + height +
                ", fillColor = " + fillColor +
                ", borderColor = " + borderColor +
                " }";
    }
}

class Triangle extends Figures implements Shape, Serializable
{
    public double getGround()
    {
        return ground;
    }

    public double getHeight()
    {
        return height;
    }

    public Color getFillColor()
    {
        return fillColor;
    }

    public Color getBorderColor()
    {
        return borderColor;
    }

    public FiguresTypes getFiguresType()
    {
        return figuresType;
    }

    private final double ground;
    private final double height;
    private Color fillColor;
    private Color borderColor;
    private FiguresTypes figuresType;

    Triangle(double ground, double height, Color fillColor, Color borderColor, FiguresTypes figuresType)
    {
        this.ground = ground;
        this.height = height;
        this.fillColor = fillColor;
        this.borderColor = borderColor;
        this.figuresType = figuresType;
        if (ground <= 0 || height <= 0)
        {
            throw new IllegalArgumentException("Parameters can't be <= 0");
        }
    }

    @Override
    public double area()
    {
        return (0.5 * (ground * height));
    }

    @Override
    public Color fillColor()
    {
        return fillColor;
    }

    @Override
    public Color borderColor()
    {
        return borderColor;
    }

    @Override
    public String figure()
    {
        return "Треугольник";
    }

    @Override
    public FiguresTypes figuresType()
    {
        return figuresType;
    }

    @Override
    public String toString()
    {
        return "Triangle" +
                " { " +
                "ground = " + ground +
                ", height = " + height +
                ", fillColor = " + fillColor +
                ", borderColor = " + borderColor +
                " }";
    }
}
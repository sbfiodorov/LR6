import java.util.*;
import java.util.stream.Stream;

class ShapeCollector
{
    Shape findMinArea(List<Shape> figures)
    {
        return figures.stream().min(Comparator.comparing(Shape::area)).orElse(null);
    }

    Shape findMaxArea(List<Shape> figures)
    {
        return figures.stream().max(Comparator.comparing(Shape::area)).orElse(null);
    }

    double allFiguresSquare(List<Shape> figures)
    {
        double allArea = 0;
        for (Shape figure : figures)
        {
            allArea += figure.area();
        }
        return allArea;
    }

    void findByFillColor(List<Shape> figures)
    {
        Stream<Shape> stream = figures.stream();
        stream.filter(s -> s.fillColor() == Color.AQUAMARINE).forEach(System.out::println);
    }

    void findByBorderColor(List<Shape> figures)
    {
        Stream<Shape> stream = figures.stream();
        stream.filter(s -> s.borderColor() == Color.LIGHT_GREEN).forEach(System.out::println);
    }

    void printFigures(List<Shape> figures)
    {
        figures.forEach(System.out::println);
    }

    int printCountFigures(List<Shape> figures)
    {
        return figures.size();
    }

    void figuresForFillColor(Map<Color, ArrayList<Shape>> map, List<Shape> figures)
    {
        for (Shape shape : figures)
        {
            if (map.containsKey(shape.fillColor()))
            {
                map.get(shape.fillColor()).add(shape);
            }
            else
            {
                map.put(shape.fillColor(), new ArrayList<>(Collections.singletonList(shape)));
            }
        }

        for (Map.Entry<Color, ArrayList<Shape>> entry : map.entrySet())
        {
            System.out.println(entry.getKey() + " :" + entry.getValue());
        }
    }

    void figuresForBorderColor(HashMap<Color, ArrayList<Shape>> map, List<Shape> figures)
    {
        for (Shape shape : figures)
        {
            if (map.containsKey(shape.borderColor()))
            {
                map.get(shape.borderColor()).add(shape);
            }
            else
            {
                map.put(shape.borderColor(), new ArrayList<>(Collections.singletonList(shape)));
            }
        }

        for (Map.Entry<Color, ArrayList<Shape>> entry : map.entrySet())
        {
            System.out.println(entry.getKey() + " :" + entry.getValue());
        }
    }

    void figuresForType(HashMap<FiguresTypes, ArrayList<Shape>> map, List<Shape> figures)
    {
        for (Shape shape : figures)
        {
            if (map.containsKey(shape.figuresType()))
            {
                map.get(shape.figuresType()).add(shape);
            }
            else
            {
                map.put(shape.figuresType(), new ArrayList<>(Collections.singletonList(shape)));
            }
        }

        for (Map.Entry<FiguresTypes, ArrayList<Shape>> entry : map.entrySet())
        {
            System.out.println(entry.getKey() + " :" + entry.getValue());
        }
    }
}
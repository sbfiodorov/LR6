import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

class ReadObject
{
    void run()
    {
        try
        {
            ObjectMapper om = new ObjectMapper();

            Charset charset = StandardCharsets.UTF_8;
            String a = Files.readString(Paths.get("data/output.json"), charset);

            String js = om.writeValueAsString(a);
            Files.write(Path.of("data/output2.json"), js.getBytes());
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

class WriteObject
{
    void run(List<Shape> figures)
    {
        try
        {
            ObjectMapper om = new ObjectMapper();
            figures.add(new Circle(10, Color.CORAL, Color.GREEN_YELLOW, FiguresTypes.CIRCLE));
            figures.add(new Square(150, Color.WHITE_SMOKE, Color.TOMATO, FiguresTypes.SQUARE));
            figures.add(new Rectangle(10, 12, Color.GOLD, Color.GREEN_SEA, FiguresTypes.RECTANGLE));

            String js = om.writeValueAsString(figures);
            Charset charset = StandardCharsets.UTF_8;
            Files.writeString(Paths.get("data/output.json"), js, charset);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}